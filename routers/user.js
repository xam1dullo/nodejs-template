const express = require('express')
const router = express.Router()
const userController = require('../controller/UserController')
router.get('/',userController.homePages  )
router.get('/links',userController.links );

router.get('/list', userController.list);

router.get('/table', userController.table);

//our alert message midleware
function messages(req,res,next){
    var message;
    res.locals.message = message;
    next();
}

router.get('/form',messages,userController.formPages);

router.post('/form', userController.form);

module.exports = router;