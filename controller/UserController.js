exports.homePages = (req, res)=> {   
    res.render('pages/home')
}
exports.links = (req, res)=> {
    //array with items to send
    
    const items = [
        {name:'node.js',url:'https://nodejs.org/en/'},
        {name:'ejs',url:'https://ejs.co'},
        {name:'expressjs',url:'https://expressjs.com'},
    ];
    
    res.render('pages/links',{
        links:items
    })
}
exports.list = (req, res)=> {
    //array with items to send
    var items = ['node.js','expressjs','ejs','javascript','bootstarp'];
    res.render('pages/list',{
        list:items
    })
}

exports.table =(req, res)=> {
    //array with items to send
    var items = [
        {name:'node.js',url:'https://nodejs.org/en/'},
        {name:'ejs',url:'https://ejs.co'},
        {name:'expressjs',url:'https://expressjs.com'},];
        
        res.render('pages/table',{
            table:items
        })
    }
    
    exports.formPages = (req, res)=> {
        res.render('pages/form');
    }
    exports.form = (req, res)=> {
        var message=req.body;
        res.locals.message = message;
        res.render('pages/form');
    }