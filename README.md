# nodejs-template

```
git clone git@gitlab.com:xam1dullo/nodejs-template.git
```
or 
```
git clone https://gitlab.com/xam1dullo/nodejs-template.git
```


- Database / Data store: MongoDB,
- Server app: Node.js, Express.js
- Views: EJS

## Installation

`
npm install
`


## Getting started

The application will start by running `node index.js` in the command line.